'use strict';

(function() {

	var _ = require('lodash');

	var mongoose = require('mongoose');

	var ObjectId = mongoose.Schema.Types.ObjectId;

	var version = require('semver');

	var handler = require('./alptekinTwofishHandler')();

	var plugin = module.exports = function(schema, config) {


		var excluded, encrypted, authenticated, isMigrating;   

		var initialAuthenticated = ['_id', '_cipher'];

		function init() {
			// Wir erwarten node version groesser als 4.0.0
			// und mongoose version groesser als 4.2.0 -> check for it
			if (version.gt(process.version, '4.0.0')) {    
				if (version.lt(mongoose.version, '4.2.0')) {
					throw new Error('Mongoose Version erwartet größer als 4.2.0');

				};
			} else {
				throw new Error('Node Version erwartet größer als 4.0.0');
			};

			// adde das _cipher feld zum schema
			if (!schema.paths._cipher) {
				schema.add({
					_cipher: {
						type: Buffer
					}
				});
			} else {
				throw new Error('_cipher darf nicht selbst definiert sein, wird von Plugin gebraucht');
			}

			// adde das _signature zum schema
			if (!schema.paths._signature) {
				schema.add({
					_signature: {
						type: Buffer
					}
				});
			} else {
				throw new Error('_signature darf nicht selbst definiert sein, wird von Plugin gebraucht');
			}

			// wir erlauben es nur top level felder auszunehmen
			if (_.some(config.excluded, function(key) {
					return key.indexOf('.') !== -1;
				})) {
				throw new Error('Es können nur Felder der obersten Hierarchie ausgeschlossen werden');
			}
			
			// adde die vom anwendungsentwickler optional von der verschluesselung
			// auszumehmenden felder zu den immer auszunehmenden feldern
			excluded = _.union(['_id', '_cipher'], config.excluded);

			// lese alle zu verschluesselnden felder aus den im schema definierten
			// feldern aus
			encrypted = _.chain(schema.paths)
				// ignoriere alle felder mit einem index
				.filter(function(details) {
					return !details._index;
				})
				// im schema path
				.map('path')
				// entferne die auszunehmenden feldern
				.difference(excluded)
				// entferne subfelder
				.map(function(path) {
					return path.split('.')[0];
				})
				// felder muessen unique sein
				.uniq()
				// wir sind fertig, also evaluiere ergebnis
				.value();
		
			// sind zusaetzlich zu signierende felder gewuenscht, fuege sie
			// zu den immer zu signierenden feldern hinzu
			if (config.authenticated) {
				authenticated = _.union(initialAuthenticated, config.authenticated);
			} else {
				authenticated = initialAuthenticated;
			}
			
			// wenn wir migrieren, exclude die pre init und pre save funktionen
			// also festhalten ob wir migrieren oder nicht
			isMigrating = config.migration || false;
		};
		init();

		// fuege dem schema eine encrypt methode hinzu
		// hier werden die vom handler auszufuehrenden schritte definiert
		schema.methods.encrypt = function() {
			var that = this;

			// verschluessele
			return handler.processEncryption(that, config.encryptionKey, encrypted)
				.then(function(doc) {
					that = doc;
					// anschliessend signiere
					return handler.processSignature(doc, config.encryptionKey, authenticated);
				});
		}

		// fuege dem schema eine decrypt methode hinzu
		// hier werden die vom handler auszufuehrenden schritte definiert
		schema.methods.decrypt = function(data) {
			var doc = null;
			// wenn data gesetzt ist, dann kommen wir von der pre init funktion
			// im pre init ist noch kein volles dokument mit daten defniert, nur roh daten liegen vor
			// ansonsten ist es ein manueller aufruf aus dem api controller
			if (data) {
				doc = data;
			} else {
				doc = this;
			}
			// authentifiziere das dokument
			return handler.processAuthentication(doc, config.encryptionKey, this.constructor.modelName)
				.then(function() {
					// ist es authentisch, dann entschluessele es
					// sollte ein fehler aufgetreten sein wurde es automatisch weitergerreicht
					return handler.processDecryption(doc, config.encryptionKey)
				});
		}

		// wenn wir nicht migrieren, adde die pre save und pre init funktion
		if (!isMigrating) {
			schema.pre('save', function(next) {
				var that = this;
				// vor dem speichern verschluessele und signiere
				that.encrypt()
					.then(function(doc) {
						that = doc;
						next();
					})
					.catch(function(error) {
						next(error);
					});
			});

			schema.pre('init', function(next, data) {
				var that = this;
				// vor der initialisierung, nach dem laden der daten
				// authentifiziere und entschluessele
				that.decrypt(data)
					.then(function(doc) {
						that = doc;
						next();
						return that;
					})
					.catch(function(error) {
						return next(error);
					});
			})
		}
	};

	// modul zur datenmigration
	module.exports.migrate = function(schema, config) {
		// wir migrieren also setzte migration auf true
		config.migration = true;
		
		// nun lade die eigentliche plugin definition
		plugin(schema, config);

		// da wir migrieren, adde eine migrate funktion zum schema
		schema.statics.migrate = function() {
			var thisSchema = this;
			
			// deligiere an die handler migrate funktion
			return handler.migrate(thisSchema);
		}
	}

})();