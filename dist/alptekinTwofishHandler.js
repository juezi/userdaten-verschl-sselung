'use strict';

(function() {
	var Promise = require('bluebird');
	var _ = require('lodash');
	var mcrypt = require('mcrypt').MCrypt;
	var crypto = require('crypto');
	var stableStringify = require('json-stable-stringify');
	var bufferEqual = require('buffer-equal-constant-time');
	var ENCRYPTION_ALGORITHM = 'twofish';
	var IV_LENGTH = 16;
	var SIGNATURE_LENGTH = 32;
	var VERSION = 'alpt'
	var VERSION_LENGTH = 4;
	var VERSION_BUFFER = new Buffer(VERSION);

	module.exports = function() {
		var handler = {};

		// methode zum generieren des signaturschluessels
		// er wird aus dem verschluesselungsschluessel abgeleitet
		// nach HMAC-SHA512
		var generateSignatureKey = function(key) {
			key = new Buffer(key);
			var hmac = crypto.createHmac('sha512', key);
			hmac.update(VERSION);
			return new Buffer(hmac.digest());
		};

		// methode zum generieren eines kryptografisch zufaelligen
		// schluessels ueber node crypto
		var generateIV = function() {
			return new Promise(function(resolve, reject) {
				crypto.randomBytes(IV_LENGTH, function(ex, buffer) {
					if (ex) {
						reject(ex);
					} else {
						resolve(buffer.toString('base64').slice(0, IV_LENGTH));
					}
				});

			});
		};

		// methode zum ausfuehren der twofish verschluesselung
		handler.processEncryption = function(doc, encryptionKey, fields) {
			return new Promise(function(resolve, reject) {
				// lese die zu verschluesselnden daten aus dem dokument aus
				// loesche alle felder die kein value haben
				var object = _.pick(doc, fields);
				_.forEach(object, function(key, value) {
					if (value === undefined) {
						delete object[key];
					}
				});

				// erzeuge den kryptografisch zufaelligen IV
				generateIV()
					.then(function(iv) {

						// anschliessend ueberfuehre die JSON daten in einen string
						var objectString = JSON.stringify(object);
						// twofish verschluesselungskonfiguration -> cbc 
						var twofishCBC = new mcrypt('twofish', 'cbc');
						twofishCBC.open(encryptionKey, iv);

						// eigentliche verschluesselung des datenstrings
						try {
							var cipher = twofishCBC.encrypt(objectString);
						} catch (ex) {
							reject(ex);
							return;
						}

						// das cipher feld besteht aus 4 byte version, 16 byte iv und anschliessend dem cipherbuffer
						cipher = Buffer.concat([VERSION_BUFFER, new Buffer(iv), cipher]);

						// fuege den cipherbuffer an das dokument an
						doc._cipher = cipher;
						// loesche alle klartextdaten
						_.forEach(fields, function(key, value) {
								doc[key] = undefined;
							})
							// finish success
						resolve(doc);
					});
			});

		};

		// methode zum erstellen der signatur
		handler.processSignature = function(doc, encryptionKey, fields) {
			return new Promise(function(resolve, reject) {
				// erstelle den signaturschluessel nach PBKDF2
				var signatureKey = generateSignatureKey(encryptionKey);
				// erstelle den HMAC-SHA512 aus dem signaturschluessel
				var hmac = crypto.createHmac('sha512', signatureKey);
				// lese die zu signierenden felder aus dem dokument aus
				var object = _.pick((doc.toObject ? doc.toObject() : doc), fields);
				// erstelle einen datenstring aus den JSON daten
				var objectString = stableStringify(object);

				// update den HMAC-SHA512 mit dem namen des schemas
				// der versionsnummer des plugins
				// dem oben erzeugten datenstring
				// sowie den fuer die signatur benutzten feldern
				hmac.update(doc.constructor.modelName);
				hmac.update(VERSION);
				hmac.update(objectString);
				hmac.update(JSON.stringify(fields));

				// kuerze die signatur auf 32 byte				
				var signature32 = new Buffer(SIGNATURE_LENGTH);

				var signature64 = new Buffer(hmac.digest());
				signature64.copy(signature32, 0, 0, SIGNATURE_LENGTH);

				// die eigentliche signatur besteht aus der Version, der eigentlichen signatur
				// und den zuer erzeugung benutzten feldern
				var _signature = Buffer.concat([VERSION_BUFFER, signature32, new Buffer(JSON.stringify(fields))]);
				// fuege die signatur an das dokument an
				doc._signature = _signature;

				// finish success
				resolve(doc);

			});
		};

		// methode zur authentifizierung eines dokumentes
		handler.processAuthentication = function(doc, encryptionKey, schemaname) {
			return new Promise(function(resolve, reject) {
				// lese das signaturfeld aus dem dokument aus
				var _signature = doc._signature.hasOwnProperty('buffer') ? doc._signature.buffer : doc._signature;

				// ist die laenge der signatur viel zu klein
				// dann kann das dokument nicht authentisch sein
				if (_signature.length < VERSION_LENGTH + SIGNATURE_LENGTH + 2) {
					reject(new Error('Dokument ist nicht authentisch!'));
				}

				// Lese eigentliche Signatur aus
				// parse die version vom anfang des signatur feldes
				var versionUsed = _signature.slice(0, VERSION_LENGTH).toString();
				// parse die eigentliche signatur
				var signature = _signature.slice(VERSION_LENGTH, VERSION_LENGTH + SIGNATURE_LENGTH);
				// parse die zur signierung benutzten felder
				var fields = JSON.parse(_signature.slice(VERSION_LENGTH + SIGNATURE_LENGTH, _signature.length).toString());

				// Erzeuge pruefsignatur ueber die zur signierung benutzten felder
				// mit dem aus der db geladenen dokument -> wie vorher bei signaturerzeugung
				var signatureKey = generateSignatureKey(encryptionKey);
				var hmac = crypto.createHmac('sha512', signatureKey);
				var object = _.pick((doc.toObject ? doc.toObject() : doc), fields);
				var objectString = stableStringify(object);

				hmac.update(schemaname);
				hmac.update(VERSION);
				hmac.update(objectString);
				hmac.update(JSON.stringify(fields));

				// die erwartete signatur
				var expectedSignatureShort = new Buffer(SIGNATURE_LENGTH);

				var expectedSignatureLong = new Buffer(hmac.digest());
				expectedSignatureLong.copy(expectedSignatureShort, 0, 0, SIGNATURE_LENGTH);

				// Vergleiche erwartete mit eigentlicher Signatur
				if (bufferEqual(expectedSignatureShort, signature)) {
					resolve();
				} else {
					reject(new Error('Das Dokument ist nicht authentisch!'));
				}
			});
		}

		// methode zum enschluesseln eines dokumentes
		handler.processDecryption = function(doc, encryptionKey) {
			return new Promise(function(resolve, reject) {
				// ist kein cipher bzw. signature feld vorhanden, dann ist etwas schiefgelaufen
				// deshalb wird auch eine manuelle migration alter daten benoetigt, sonst kracht es hier
				if (!doc._cipher || !doc._signature) {
					reject(new Error('Benötigte Felder fehlen'));
					return;
				}

				// lese das cipher feld aus dem dokument aus
				var _cipher = doc._cipher.hasOwnProperty('buffer') ? doc._cipher.buffer : doc._cipher;

				// lese den IV aus dem cipher buffer aus
				var iv = _cipher.slice(VERSION_LENGTH, VERSION_LENGTH + IV_LENGTH);
				// die eigentliche cipher ist dann das _cipher feld abzueglich des IV
				var cipher = _cipher.slice(VERSION_LENGTH + IV_LENGTH, _cipher.length);

				// twofish konfiguration mit cbc und iv
				var twofishCBC = new mcrypt('twofish', 'cbc');
				twofishCBC.open(encryptionKey, iv);

				// die eigentliche entschluesselung
				try {
					var objectString = twofishCBC.decrypt(cipher).toString();
				} catch (ex) {
					reject(ex);
					return;
				}

				// mcrypt twofish erstellt keinen korrekten json string bei der entschluesselung
				// es sind noch invalide line endings etc. vorhanden
				// also muessen wir sie manuell aus dem string entfernen
				// siehe google for reference
				objectString = objectString.replace(/\\n/g, "\\n")
					.replace(/\\'/g, "\\'")
					.replace(/\\"/g, '\\"')
					.replace(/\\&/g, "\\&")
					.replace(/\\r/g, "\\r")
					.replace(/\\t/g, "\\t")
					.replace(/\\b/g, "\\b")
					.replace(/\\f/g, "\\f");
				// entferne alle non-printable und andere invalide JSON zeichen
				objectString = objectString.replace(/[\u0000-\u0019]+/g, "");
				
				// nun koennen wir den validen json string in einen json unwandeln
				var object = JSON.parse(objectString);
				
				// fuege die entschluesselten daten wieder an die korrekte stelle im dokument ein
				_.forEach(object, function(value, key) {
					doc[key] = (_.isObject(value) && value.type === 'Buffer') ? value.data : value;
				});
				
				// loesche _cipher und _signature felder aus dem dokument
				doc._cipher = undefined;
				doc._signature = undefined;

				// finish success
				resolve(doc);
			});
		}

		// methode zur migration alter dokumente/daten
		handler.migrate = function(schema) {
			return new Promise(function(resolve, reject) {
				// lade alle dokumente des schema typs
				schema.findAsync({})
					.then(function(documents) {
						// erstelle ein leeres array um die einzelnen promise der verschluesselungsfunktion
						// der einzelne dokumente zwischenzulagern
						var promises = [];
						// fuehre eine verschluesselung fuer jedes einzelne in der db vorhandene
						// dokument des schemas durch
						// die signatur wird automatisch bei der verschluesselung erzeugt
						_.forEach(documents, function(doc) {
							promises.push(doc.encrypt());
						});
						// erst wenn alle promises in dem promise array erfolgreich ausgefuehrt sind,
						// gehe zum naechsten then weiter, failed auch nur eine einzelne verschluesselungsaktion
						// wird in den catch block gewechselt
						return Promise.all(promises);
					})
					.then(function(encryptedSignedDocuments) {
						var promises = [];

						// nun signiere jedes einzelne dokument
						_.forEach(encryptedSignedDocuments, function(encryptedSignedDoc) {
							promises.push(encryptedSignedDoc.saveAsync());
						});

						return Promise.all(promises);
					})
					.then(function(success) {
						// finish success
						resolve('success');
					})
					.catch(function(error) {
						// fail errors
						reject(error);
					})
			});
		}

		return handler;
	}

})();